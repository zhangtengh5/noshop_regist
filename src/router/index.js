import Vue from 'vue'
import Router from 'vue-router'
import store from '@/vuex/store';
import { getQueryString } from '@/assets/js/handleUrl.js';
import Telephone from '@/components/telephone';
import Success from '@/components/success';
import Failed from '@/components/failed';
import Plam from '@/components/plam';
import Login from '@/components/login';
import Account from '@/components/account';
import CZSuccess from '@/components/cz_success';
import Pay from '@/components/pay';
import Money from '@/components/money';
import NoMatch from '@/components/test';
Vue.use(Router);
let router = new Router({
  routes: [
    {
      path: '/regist',
      name: 'regist',
      component: Telephone
    },
    {
      path: '/success',
      name: 'success',
      component: Success
    },
    {
      path: '/failed',
      name: 'failed',
      component: Failed
    },
    {
      path: '/plam',
      name: 'plam',
      component: Plam
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/pay',
      name: 'pay',
      component: Pay,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/czsucc',
      name: 'czsucc',
      component: CZSuccess
    },
    {
      path: '/test',
      name: 'nomatch',
      component: NoMatch
    },
    {
      path: '/account',
      name: 'account',
      component: Account,
      meta: {
         requiresAuth: true
      }
    },
    {
      path: '/money',
      name: 'money',
      component: Money,
      meta: {
        requiresAuth: true
      }
    }
  ]
})
router.beforeEach((to, from, next) => {
  store.state.equipmentID = getQueryString('equipmentID');
  if (to.path === '/') {
    store.state.accountId = null;
    store.state.userData = null;
  }
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    let accountId = store.state.accountId;
    if (!accountId) {
      next({
        path: '/'
      })
    } else {
      next()
    }
  } else {
    next() // 确保一定要调用 next()
  }
})
export default router
