/**
 * plamStep手掌注册处于的阶段
 *  0: 默认
 *  1: 开启手掌验证
 *  2: 验证手掌数据获取完成
 *  3: 验证完成开始注册
 *  4: 采集手掌数据获取完成
 */
import Vuex from 'vuex'
import http from '../axios/http';
import router from '../router';
import {Message} from 'element-ui';
import Vue from 'vue'
Vue.use(Vuex);
let store = new Vuex.Store({
    state: {
      isLogin: true,
      equipmentID: null,
      telephoneInfo: null,
      activexObj: null,
      error_title: '',
      error_message: '',
      softProgress: null,
      LoginProgress: null,
      againRegist: false,
      plamStep: 0,
      loadShow: false,
      loadTitle: '加载中...',
      accountId: null,
      userData: null,
      reTime: 0,
      oldreTime: 2,
      payMoney: {
        type: 0,
        money: 0,
        current: 0
      },
      firstCharge: false,
      myphone: ''
    },
    getters: {
      countName(state) {
        if (state.userData != null) {
          let name = state.userData.name;
          let length = name.length;
          if (length) {
            switch (length) {
              case 2:
                return name.substring(0, 1) + '*';
                break;
              case 3:
                return name.substring(0, 1) + '**';
                break;
              default:
                return name.substring(0, 1) + '***';
            }
          } else {
            return '***';
          }
        }
      },
      countIdCard(state) {
        if (state.userData != null) {
          let idCard = state.userData.idk;
          let end = idCard.length;
          if (end) {
            let start = end - 1;
            return idCard.substring(0, 1) + '****************' + idCard.substring(start, end);
          } else {
            return '***';
          }
        }
      },
      countTelephone(state) {
        if (state.userData != null) {
          let telephone = state.userData.account;
          if (telephone.length) {
            return telephone.substring(0, 3) + '****' + telephone.substring(7);
          } else {
            return '***';
          }
        }
      },
      countMoney(state) {
        if (state.userData != null) {
          let money = state.userData.handMoney;
          if (money > 0) {
            return money.toFixed(2);
          } else {
            return money
          }
        }
      },
      countBankCard(state) {
        if (state.userData != null) {
          let bankCard = state.userData.backNo;
          let end = bankCard.length;
          if (end) {
            let start = end - 4;
            return '**** **** **** ' + bankCard.substring(start, end);
          } else {
            return '';
          }
        }
      },
      countScore(state) {
        if (state.userData != null) {
          return state.userData.integral;
        }
      },
      myTelephone(state) {
        let telephone = state.myphone;
        if (telephone.length) {
          return telephone.substring(0, 3) + '****' + telephone.substring(7);
        } else {
          return '***';
        }
      }
    },
    mutations: {
      gotMyInfo(state, payload) {
        let values = payload.value;
        state.userData = values.userData;
        state.firstCharge = values.firstCharge;
        state.myphone = values.userData.account;
      },
      goPay(state, payload) {
        state.payMoney = payload.payMoney;
        router.push('/money');
      },
      startRegist (state, payload) {
        //注册开启手掌验证
        state.isLogin = false;
        state.telephoneInfo = payload.telephoneInfo;
        router.push('/plam');
        state.activexObj.capturePalmData();
        state.plamStep = 1;
      },
      startLogin (state, payload) {
        state.isLogin = true;
        router.push('/login');
        state.activexObj.capturePalmData();
        state.plamStep = 1;
      },
      capturePalmData (state, payload) {
        //验证手掌数据获取完成
        state.plamStep = 2; 
        let verifyData = payload.verifyData;
        http.$ajax('/vein/find.do', {
          veinData: verifyData,
          equipmentName: state.equipmentID
        }).then((result) => {
          if (parseInt(result.result) === 0) {
            //验证完成掌静脉已存在
            if (state.isLogin) {
              //登录成功
              state.accountId = result.id;
              //Message.success(result.id);
              if (state.accountId) {
                router.push('/account');
              } else {
                //掌静脉Id不存在
                state.error_title = '抱歉，查询失败！';
                state.error_message = '您的掌静脉信息不存在';
                router.push('/failed');
              }
              state.plamStep = 0;
              state.reTime = 0;
            } else {
              //注册失败
              state.error_title = '抱歉，注册失败！';
              state.error_message = '您的掌静脉信息已经被注册';
              router.push('/failed');
              window.external.playMusic("6");
              state.plamStep = 0;
            }
          } else {
            //验证完成掌静脉不存在
            if (state.isLogin) {
              //登录失败
              state.reTime++;
              //两次验证
              if (state.reTime > state.oldreTime) {
                state.reTime = 0;
                state.plamStep = 0;
                state.error_title = '抱歉，查询失败！';
                state.error_message = '未识别到您的掌静脉信息';
                router.push('/failed');
              } else {
                state.activexObj.capturePalmData(); //重新登录
                state.plamStep = 1;
              }
            } else {
              //开始注册
              state.activexObj.enrollBtnClick();
              state.plamStep = 3;
            }
          }
        }).catch((error)=> {
          Message.error(error);
        })
      },
      enrollData (state, payload) {
        //采集手掌数据获取完成
        state.plamStep = 4; 
        let registData = payload.registData;
        let telephoneInfo = state.telephoneInfo;
        http.post('/userInfo/MiddleAddUser.do', {
          equipmentName: state.equipmentID,
          phone: telephoneInfo.phone_num,
          recPhone: telephoneInfo.recommend_num,
          vien: registData
        }).then((result) => {
          if (parseInt(result.status) === 1) {
            //注册成功
            state.firstCharge = result.firstCharge;
            state.accountId = result.userid;
            state.myphone = result.phone;
            router.push('/success');
          } else {
             //注册失败
            state.error_title = '抱歉，注册失败！';
            state.error_message = result.msg;
            router.push('/failed');
          }
          state.plamStep = 0;
        }).catch((error) => {
          Message.error(error);
        })
      },
      registStatus (state, payload) {
        //注册过程的状态信息
        let statusInfo = payload.statusInfo;
        let softProgress = state.softProgress;
        let LoginProgress = state.LoginProgress;
        if (state.isLogin) {
          LoginProgress(statusInfo);
        } else {
          softProgress(statusInfo);
        }
      },
      againRegistPalm (state, payload) {
        //重新注册
        if (state.plamStep === 1) {
          state.againRegist = true;
          state.activexObj.cancleCapturePalm();//取消验证
        } else if (state.plamStep === 3) {
          state.againRegist = true;
          state.activexObj.cancleEnroll();//取消注册
        } else {
          state.activexObj.capturePalmData(); //开启验证
          state.plamStep = 1;
        }
      },
      cancleSoft (state, payload) {
        //取消注册
        if (state.plamStep === 1) {
          state.activexObj.cancleCapturePalm();//取消验证
        } else if (state.plamStep === 3) {
          state.activexObj.cancleEnroll();//取消注册
        }
        state.plamStep = 0;
        state.reTime = 0; //验证次数统计重置
      },
      cancleAllSoft (state, payload) {
        //返回
        state.activexObj.cancleEnroll();//取消注册
        state.activexObj.cancleCapturePalm();//取消验证
      },
      finishSoft (state, payload) {
        state.activexObj.Ps_Sample_Apl_CS_FinishLibrary();//关闭掌静脉
      }
    }
})

export default store