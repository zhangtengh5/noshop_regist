// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './vuex/store';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import { Notification, Message, MessageBox} from 'element-ui';
import 'swiper/dist/css/swiper.css';
import './assets/scss/base.scss';
import animated from 'animate.css'
Vue.use(animated)
Vue.use(VueAwesomeSwiper);
Vue.prototype.$notify = Notification;
Vue.prototype.$message = Message;
Vue.prototype.$alert = MessageBox.alert;
Vue.config.productionTip = false;
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
